"use strict";

// Look for .hamburger
var hamburger = document.querySelector(".hamburger"); // On click

hamburger.addEventListener("click", function () {
  // Toggle class "is-active"
  hamburger.classList.toggle("is-active"); // Do something else, like open/close menu
});
"use strict";

function navBar() {
  var x = document.getElementById("site-header-main");

  if (x.className === "site-header__main") {
    x.className += " site-header__main--active";
  } else {
    x.className = "site-header__main";
  }
}
"use strict";

jQuery(function ($) {
  $('.header-slideshow').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 6200,
    fade: true,
    speed: 3000,
    infinite: true
  });
});
"use strict";

jQuery(function ($) {
  $(function () {
    $('<a name="top"/>').insertBefore($('body').children().eq(0));
    window.location.hash = 'top';
  });
});