function navBar() {
  var x = document.getElementById("site-header-main");
  if (x.className === "site-header__main") {
    x.className += " site-header__main--active";
  } else {
    x.className = "site-header__main";
  }
}
