jQuery(function ($) {

	$('.header-slideshow').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 6200,
		fade: true,
		speed: 3000,
		infinite: true,
	});
});
