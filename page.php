<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Professionele_Identiteit
 */

get_header();
?>

<main id="primary" class="site-main">
	<?php get_template_part('template-parts/layout/header/header', 'site'); //header
	?>

	<section class="content_text_section" style="display: inline-block;">
		<div class="breadcrumbs"> <?php
															if (function_exists('yoast_breadcrumb')) {
																yoast_breadcrumb('<p id="breadcrumbs">', '</p>');
															}
															?>
		</div>
		<div class="content-text-container col-md-8">
			<div class="row">
				<div class="page-content-links col-md-2">
					<?php if (get_field('navigation_menu')) : ?>
						<?php // Loads selected menu
						$menu_select = get_field('navigation_menu');
						bem_menu($menu_select);
						?>
					<?php endif; ?>
				</div>

				<?php the_content(); ?>


	</section>

	<section class="footer-section">
		<?php get_template_part('template-parts/layout/footer/footer', 'site'); //footer
		?>
</main>
<?php
get_footer();
