
<div class="footer_container container">
<div class="d-flex flex-row-reverse">
<div class="p-2"> <a href="#top"><img id="arrow" src="https://fonts.gstatic.com/s/i/materialiconsoutlined/arrow_upward/v5/24px.svg"></a></div>
	<div class="p-2"><?php the_field("footer_1", 'option') ?></div>
	<div class="p-2"><?php the_field("footer_0", 'option') ?></div>
</div> <!--row-->
</section>
