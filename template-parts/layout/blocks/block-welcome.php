<section class="welcome">
		<div class="container">
			<div class="row">
				<div class="col-md-9" id="welcome">
					<?php the_field('welcome'); ?>
				</div>
			</div>
		</div>
	</section>
