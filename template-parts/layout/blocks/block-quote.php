<section class="quote">
	<div class="container">
		<div class="row">
			<div class="col-md-12" id="quote">
				<h2><?php the_field('quote_text', 'option'); ?></h2>
			</div>
			<div class="col-md-12">
				<button type="button" class="btn btn-primary"><?php the_field('button_text', 'option'); ?></button>
			</div>
		</div>
	</div>
</section>
