<section class="points">
	<div class="container">
		<div class="row">
			<?php if (have_rows('points')) : ?>
				<?php while (have_rows('points')) : the_row(); ?>
					<div class="content_points col-md-4 col-12">
						<?php the_sub_field('point_editor'); ?>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>
