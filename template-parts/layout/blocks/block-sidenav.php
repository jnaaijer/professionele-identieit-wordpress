<div class="page-content-links col-md-2">
					<?php if (get_field('navigation_menu')) : ?>
						<?php // Loads selected menu
						$menu_select = get_field('navigation_menu');
						bem_menu($menu_select);
						?>
					<?php endif; ?>
				</div>
