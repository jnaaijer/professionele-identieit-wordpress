	<header class="site-header">
		<div class="site-header__top" id="header">
			<div class="site-header__logo">
				<img src="#" alt="">
			</div>

			<div class="site-header__controls">
				<button class="hamburger hamburger--collapse" type="button" onClick="navBar()">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
			</div>
		</div>
		<div class="site-header__main" id="site-header-main">
			<?php bem_menu('menu-1'); ?>
		</div>
	</header>



	<div class="container-header">

		<header class="header">
			<div class="header-slideshow">

				<?php if (have_rows('headerslides', 'option')) : ?>
					<?php while (have_rows('headerslides', 'option')) : the_row(); ?>
						<div>

							<?php
							$image = get_sub_field('header_slide', 'option');
							$size = 'full'; // (thumbnail, medium, large, full or custom size)
							if ($image) { ?>
								<img class="slide-image" src="<?php echo wp_get_attachment_image_url($image, $size); ?>" alt="">
							<?php } ?>
						</div>
					<?php endwhile; ?>
				<?php endif; ?>



			</div>

			<div class="row" id="headerrow" style="height: 100%">
				<div class="col-md-12" id="headertext">
					<h1><?php the_field('headertext', 'option'); ?></h1>
				</div>
			</div>
			<div class="header-divider">

			</div>
	</div>

	</header>
	</div>
