<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Professionele_Identiteit
 */

get_header();
?>

	<main id="primary" class="site-main">

		<section class="error-404 not-found">

			<h1>Error 404: Pagina niet gevonden :(</h1>
			<a href="/home" style="color: black;">Terug naar home</a>

			</div><!-- .page-content -->
		</section><!-- .error-404 -->

	</main><!-- #main -->

<?php
get_footer();
