# Info
Wordpress thema voor Professionele Identiteit (pi.ahwageningen.nl)

## Benodigdheden (w.i.p)

Package manager: Node.JS voor NPM/NPX 	https://nodejs.org

Als je dit thema pulled: verwijder dan node_templates en package-lock.json voordat je npm install doet.


- WP-Gulp 															https://github.com/ahmadawais/WPGulp
```bash
npx wpgulp
```

- Bootstrap															https://getbootstrap.com/
```bash
npm install bootstrap
```
- Slick-Carousel 												https://kenwheeler.github.io/slick/
```bash
npm install slick-carousel
```

- Hamburgers 												https://jonsuh.com/hamburgers/
```bash
npm install hamburgers
```

Je kan ook alles in 1 keer binnen halen met:

```bash
npm install
```

## Plugins

- ACF Pro, voor velden en content (Premium plugin)
- Yoast SEO voor Breadcrumbs/Optimalisatie.
- Gravity Forms voor contactforumulier (Premium plugin)
- W3Total Cache
- Query Monitor (optioneel)
- Classic Editor (optioneel) werkt iets fijner i.c.m. ACF
