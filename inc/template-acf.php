<?php

/**
 * Advanced Custom Fields, register blocks
 *
 * @package Foundation
 */

/*
|----------------------------------------------------------------
| Advanced Custom Fields - blocks
|----------------------------------------------------------------
*/
function register_acf_block_types()
{


	acf_register_block_type(array( //points
		'name'            => 'points',
		// 'icon'         => 'admin-comments',
		'title'           => __('Points', 'foundation'),
		'render_template' => 'template-parts/layout/blocks/block-points.php',
		'supports'        => array('align' => false, 'mode' => false),
		'mode'            => 'edit',
	));

	acf_register_block_type(array( //welcome
		'name'            => 'welcome',
		// 'icon'         => 'admin-comments',
		'title'           => __('Welcome', 'foundation'),
		'render_template' => 'template-parts/layout/blocks/block-welcome.php',
		'supports'        => array('align' => false, 'mode' => false),
		'mode'            => 'edit',
	));

	acf_register_block_type(array( //Quote
		'name'            => 'quote',
		// 'icon'         => 'admin-comments',
		'title'           => __('Quote', 'foundation'),
		'render_template' => 'template-parts/layout/blocks/block-quote.php',
		'supports'        => array('align' => false, 'mode' => false),
		'mode'            => 'edit',
	));

	acf_register_block_type(array( //page content
		'name'            => 'page-content',
		// 'icon'         => 'admin-comments',
		'title'           => __('Page content', 'foundation'),
		'render_template' => 'template-parts/layout/blocks/block-page-content.php',
		'supports'        => array('align' => false, 'mode' => false),
		'mode'            => 'edit',
	));

	acf_register_block_type(array( //contact
		'name'            => 'contact-page',
		// 'icon'         => 'admin-comments',
		'title'           => __('Contact Page', 'foundation'),
		'render_template' => 'template-parts/layout/blocks/block-page-contact.php',
		'supports'        => array('align' => false, 'mode' => false),
		'mode'            => 'edit',
	));
}

// Check if function exists and hook into setup.
if (function_exists('acf_register_block_type')) {
	add_action('acf/init', 'register_acf_block_types');
}

function filter_block_types( $allowed_blocks ) {
	return array (
		'acf/points',
		'acf/welcome',
		'acf/page-content',
		'acf/contact-page',
		'acf/quote',
	);
}
add_filter('allowed_block_types', 'filter_block_types');
