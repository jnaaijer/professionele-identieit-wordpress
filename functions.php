<?php

/**
 * Professionele_Identiteit functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Professionele_Identiteit
 */

if (!defined('_S_VERSION')) {
	// Replace the version number of the theme on each release.
	define('_S_VERSION', '1.0.0');
}

if (!function_exists('professionele_identiteit_setup')) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function professionele_identiteit_setup()
	{
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Professionele_Identiteit, use a find and replace
		 * to change 'professionele_identiteit' to the name of your theme in all the template files.
		 */
		load_theme_textdomain('professionele_identiteit', get_template_directory() . '/languages');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support('title-tag');

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support('post-thumbnails');

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__('Primary', 'professionele_identiteit'),
				'gedachtegoed' => esc_html__('Gedachtegoed', 'professionele_identiteit'),
				'ontwikkelen' => esc_html__('Ontwikkelen', 'professionele_identiteit'),
				'producten en publicaties' => esc_html__('Producten en publicaties', 'professionele_identiteit'),
				'samenwerken' => esc_html__('Samenwerken', 'professionele_identiteit')
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'professionele_identiteit_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action('after_setup_theme', 'professionele_identiteit_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function professionele_identiteit_content_width()
{
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters('professionele_identiteit_content_width', 640);
}
add_action('after_setup_theme', 'professionele_identiteit_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function professionele_identiteit_widgets_init()
{
	register_sidebar(
		array(
			'name'          => esc_html__('Sidebar', 'professionele_identiteit'),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'professionele_identiteit'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'professionele_identiteit_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function professionele_identiteit_scripts()
{
	wp_enqueue_style('professionele_identiteit-style', get_stylesheet_uri(), array(), _S_VERSION);
	wp_style_add_data('professionele_identiteit-style', 'rtl', 'replace');

	wp_enqueue_script('professionele_identiteit-vendor', get_template_directory_uri() . '/assets/js/vendor.min.js', array(), _S_VERSION, true);
	wp_enqueue_script('professionele_identiteit-custom', get_template_directory_uri() . '/assets/js/custom.min.js', array(), _S_VERSION, true);

	wp_enqueue_script('professionele_identiteit-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);
	wp_enqueue_script('jquery');

	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'professionele_identiteit_scripts');

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';
require get_template_directory() . '/inc/template-walker.php';
require get_template_directory() . '/inc/template-acf.php';
